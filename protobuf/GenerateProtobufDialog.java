package protobuf;

import static protobuf.options.OptionsUtils.readCompilers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import org.gjt.sp.jedit.GUIUtilities;
import org.gjt.sp.jedit.jEdit;
import org.gjt.sp.jedit.browser.VFSBrowser;
import org.gjt.sp.util.IOUtilities;
import org.gjt.sp.util.Log;
import org.gjt.sp.util.Task;
import org.gjt.sp.util.ThreadUtilities;

import protobuf.options.ProtoCompiler;

public class GenerateProtobufDialog  extends JDialog{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1524933969455268868L;

	public static final String OPTIONS = "options.protobuf.";
	public static final String MESSAGE = "messages.protobuf.";
	private static final String GEOMETRY = OPTIONS + "GenerateProtobufDialog";
	private static final String PROTO_PATH_OPTION = "--proto_path=";
	private static final String CPP_PATH_OPTION = "--cpp_out=";
	private static final String JAVA_PATH_OPTION = "--java_out=";
	private static final String PYTHON_PATH_OPTION = "--python_out=";
	
	private final JComboBox<String> protocList;
	//private JTextArea proto;
	private FileSelectionPanel protoFilePanel = new FileSelectionPanel("protobuf.selectProtoFile", 40);
	private FolderSelectionPanel cppPathPanel = new FolderSelectionPanel("protobuf.selectCppPath", 40);
	private FolderSelectionPanel javaPathPanel = new FolderSelectionPanel("protobuf.selectJavaPath", 40);
	private FolderSelectionPanel pythonPathPanel = new FolderSelectionPanel("protobuf.selectPythonPath", 40);

	private JTextArea result;
	private JCheckBox cppSelected;
	private JCheckBox javaSelected;
	private JCheckBox pythonSelected;
	private ProtoPathTableModel protoPathModel;

	//private DefaultErrorSource errorSource = new DefaultErrorSource("XQuery plugin");

	public GenerateProtobufDialog(Frame frame) {
		this(frame,"","");
		
	}

	public GenerateProtobufDialog(Frame frame, String filePath, String fileName)
	{
		super(frame, jEdit.getProperty(MESSAGE + "GenerateProtobufDialogTitle"), true);

		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent evt)
			{
				saveGeometry();
			}
		});
		setLayout(new BorderLayout());
	
		Log.log(Log.DEBUG, this, "Display Panel");
		// Display panel
		JPanel protocPanel = new JPanel(new FlowLayout());
		JLabel protocLabel = new JLabel(jEdit.getProperty("messages.protobuf.protoc.label", ""));
		protocPanel.add(protocLabel);
		protocList = new JComboBox<>(getProtocNames(jEdit.getProperty("options.protobuf.protoc.compilers", "")));
		protocPanel.add(protocList);
		add(protocPanel, BorderLayout.NORTH);

		Log.log(Log.DEBUG, this, "Main Panel");
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main,BoxLayout.Y_AXIS));
		add(main,BorderLayout.CENTER);
		
		Log.log(Log.DEBUG, this, "Proto Panel");
		JPanel protoRow = new JPanel();
		protoRow.setLayout(new BoxLayout(protoRow,BoxLayout.X_AXIS));
		
		JPanel protoLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel protoLabel = new JLabel(jEdit.getProperty("messages.protobuf.proto.label", ""));
		protoLabelPanel.add(protoLabel);
		protoRow.add(protoLabelPanel);
		
		JPanel protoPanel1 = new JPanel(new FlowLayout());
		protoPanel1.add(protoFilePanel);
		if (fileName != null && filePath != null) {
			protoFilePanel.setSourceFieldText(filePath + fileName);
		}
		protoFilePanel.setSelectionEnabled(true);
		protoRow.add(protoPanel1);

		JPanel protoPanel2 = new JPanel(new FlowLayout());		
		protoPanel2.add(new JLabel(""));
		protoRow.add(protoPanel2);

		main.add(protoRow);

		Log.log(Log.DEBUG, this, "Proto Path Panel");
		JPanel protoPathRow = new JPanel();
		protoPathRow.setLayout(new BoxLayout(protoPathRow,BoxLayout.X_AXIS));
		
		JPanel protoPathPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel protoPathLabel = new JLabel(jEdit.getProperty("messages.protobuf.protopath.label", ""));
		protoPathPanel.add(protoPathLabel);
		protoPathRow.add(protoPathPanel);
		
        //create the model
		protoPathModel = new ProtoPathTableModel(filePath);
		
        //create the table
        final JTable protoPath = new JTable(protoPathModel);
        protoPath.setTableHeader(null);
        Dimension d = protoPath.getPreferredSize();
        JScrollPane tablePane = new JScrollPane(protoPath);  
        tablePane.setPreferredSize(new Dimension(400,protoPath.getRowHeight()*5));
        tablePane.setBorder(BorderFactory.createLineBorder(Color.GRAY,1));
        protoPathRow.add(tablePane);
		
        Log.log(Log.DEBUG,this,"Table preferred width " + d.width);
        
		JPanel protoPathButtonPanel = new JPanel();
		protoPathButtonPanel.setLayout(new BoxLayout(protoPathButtonPanel,BoxLayout.Y_AXIS));

		JPanel addPathButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		final JButton add = new JButton("Add");
		addPathButtonPanel.add(add);
		protoPathButtonPanel.add(addPathButtonPanel);

		add.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{			
				String[] selections = GUIUtilities.showVFSFileDialog(jEdit.getActiveView(), "", VFSBrowser.CHOOSE_DIRECTORY_DIALOG, false);
				protoPathModel.addPath(selections[0]);
			}
		});

		
		JPanel delPathButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		final JButton delete = new JButton("Delete");
		delPathButtonPanel.add(delete);
		protoPathButtonPanel.add(delPathButtonPanel);

		delete.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{	
				protoPathModel.removePath(protoPath.getSelectedRow());
			}
		});

		protoPathRow.add(protoPathButtonPanel);
				
		main.add(protoPathRow);

		Log.log(Log.DEBUG, this, "CPP Panel");
		JPanel cppRow = new JPanel();
		cppRow.setLayout(new BoxLayout(cppRow,BoxLayout.X_AXIS));
		
		JPanel cppPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel cppLabel = new JLabel(jEdit.getProperty("messages.protobuf.cpppath.label", ""));
		cppPanel.add(cppLabel);
		cppSelected = new JCheckBox();
		cppSelected.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cppPathPanel.setSelectionEnabled(cppSelected.isSelected());
			}
			
		});
		cppPanel.add(cppSelected);
		cppRow.add(cppPanel);

		JPanel cppPanel1 = new JPanel(new FlowLayout());
		cppPanel1.add(cppPathPanel);
		cppPathPanel.setSelectionEnabled(false);
		cppRow.add(cppPanel1);

		JPanel cppPanel2 = new JPanel(new FlowLayout());		
		cppPanel2.add(new JLabel(""));
		cppRow.add(cppPanel2);

		main.add(cppRow);
		
		Log.log(Log.DEBUG, this, "Java Panel");
		JPanel javaRow = new JPanel();
		javaRow.setLayout(new BoxLayout(javaRow,BoxLayout.X_AXIS));
		
		JPanel javaPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel javaLabel = new JLabel(jEdit.getProperty("messages.protobuf.javapath.label", ""));
		javaPanel.add(javaLabel);
		javaSelected = new JCheckBox();
		javaSelected.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				javaPathPanel.setSelectionEnabled(javaSelected.isSelected());
			}
			
		});

		javaPanel.add(javaSelected);
		javaRow.add(javaPanel);

		JPanel javaPanel1 = new JPanel(new FlowLayout());
		javaPanel1.add(javaPathPanel);
		javaPathPanel.setSelectionEnabled(false);
		javaRow.add(javaPanel1);

		JPanel javaPanel2 = new JPanel(new FlowLayout());		
		javaPanel2.add(new JLabel(""));
		javaRow.add(javaPanel2);

		main.add(javaRow);
		
		Log.log(Log.DEBUG, this, "Python Panel");
		JPanel pythonRow = new JPanel();
		pythonRow.setLayout(new BoxLayout(pythonRow,BoxLayout.X_AXIS));
		
		JPanel pythonPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel pythonLabel = new JLabel(jEdit.getProperty("messages.protobuf.pythonpath.label", ""));
		pythonPanel.add(pythonLabel);
		pythonSelected = new JCheckBox();
		pythonSelected.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				pythonPathPanel.setSelectionEnabled(pythonSelected.isSelected());
			}
			
		});

		
		pythonPanel.add(pythonSelected);
		pythonRow.add(pythonPanel);
		
		JPanel pythonPanel1 = new JPanel(new FlowLayout());
		pythonPanel1.add(pythonPathPanel);
		pythonPathPanel.setSelectionEnabled(false);
		pythonRow.add(pythonPanel1);		

		JPanel pythonPanel2 = new JPanel(new FlowLayout());		
		pythonPanel2.add(new JLabel(""));
		pythonRow.add(pythonPanel2);
		main.add(pythonRow);
		
		//Match the preferred size of the other cells in the same column
		Dimension pythonPanelSize = pythonPanel.getPreferredSize();
		protoLabelPanel.setPreferredSize(pythonPanelSize);
		protoPathPanel.setPreferredSize(pythonPanelSize);
		cppPanel.setPreferredSize(pythonPanelSize);
		javaPanel.setPreferredSize(pythonPanelSize);		

		//Match the preferred size of the other cells in the same column
		Dimension protoButtonSize = protoPathButtonPanel.getPreferredSize();
		protoPanel2.setPreferredSize(protoButtonSize);
		cppPanel2.setPreferredSize(protoButtonSize);
		javaPanel2.setPreferredSize(protoButtonSize);
		pythonPanel2.setPreferredSize(protoButtonSize);
		
		Log.log(Log.DEBUG, this, "Button Panel");

		JPanel resultAndButtonsPanel = new JPanel();
		resultAndButtonsPanel.setLayout(new BoxLayout(resultAndButtonsPanel,BoxLayout.Y_AXIS));
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.CENTER));		
		final JButton generate = new JButton("Generate");
		buttons.add(generate);
		final JButton clear = new JButton("Clear");
		buttons.add(clear);		
		resultAndButtonsPanel.add(buttons);

		JPanel resultPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		result = makeDisplayBox();
        JScrollPane resultPane = new JScrollPane(result);  
		resultPanel.add(resultPane);
		resultAndButtonsPanel.add(resultPanel);
		
		add(resultAndButtonsPanel, BorderLayout.SOUTH);
		
		generate.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{	
				result.setText("");
				
				final List<String> execCmd = new ArrayList<>();
				String protocName = protocList.getItemAt(protocList.getSelectedIndex());
				execCmd.add(getProtocExecutable(protocName));
				
				execCmd.add(protoFilePanel.getSourceFieldText());
				for (String path : protoPathModel.getPaths()) {
					execCmd.add(PROTO_PATH_OPTION + path);
				}
				
				if (cppSelected.isSelected()) {
					execCmd.add(CPP_PATH_OPTION + cppPathPanel.getSourceFieldText());
				}

				if (javaSelected.isSelected()) {
					execCmd.add(JAVA_PATH_OPTION + javaPathPanel.getSourceFieldText());
				}
				if (pythonSelected.isSelected()) {
					execCmd.add(PYTHON_PATH_OPTION + pythonPathPanel.getSourceFieldText());
				}
				Log.log(Log.DEBUG, this, execCmd);
	
				Task task = new Task()
				{
					@Override
					public void _run()
					{
						InputStreamReader esr = null;
						BufferedReader reader = null;

						try 
						{
							Log.log(Log.DEBUG, this, "Running Command");
							Log.log(Log.DEBUG, this, execCmd);
							
							final Process process = Runtime.getRuntime().exec(execCmd.toArray(new String[execCmd.size()]));

							esr = new InputStreamReader(process.getErrorStream());
							reader = new BufferedReader(esr);
							
							StringBuilder error = new StringBuilder();
							String line = reader.readLine();
							while (null != line) {
								error.append(line);
								error.append("\n");
								line = reader.readLine();
							}
							
							process.waitFor();
							
							if (error.length() > 0) {
								Log.log(Log.ERROR, this, error);
								result.setText(error.toString());
							} else {
								result.setText(jEdit.getProperty(MESSAGE + "GenerationSuccessMessage"));
							}
						} 
						catch (InterruptedException e) 
						{
							Log.log(Log.WARNING, this, " Halted by user");
							Thread.currentThread().interrupt();
							return;
						} 
						catch (IOException e) 
						{
							
							Log.log(Log.ERROR, this, e);
							Thread.currentThread().interrupt();
							return;
						}
						finally
						{
							IOUtilities.closeQuietly((Closeable)reader);
						}

					}
				};
				ThreadUtilities.runInBackground(task);		
			}
		});
		
		clear.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{	
				protocList.setSelectedIndex(-1);
				protoFilePanel.setSourceFieldText("");
				protoPathModel.clear();
				cppPathPanel.setSourceFieldText("");
				javaPathPanel.setSourceFieldText("");
				pythonPathPanel.setSourceFieldText("");
				result.setText("");
				cppSelected.setSelected(false);
				javaSelected.setSelected(false);
				pythonSelected.setSelected(false);
				
			}
		});
		
		Log.log(Log.DEBUG, this, "Load Geometry");
		GUIUtilities.loadGeometry(this, GEOMETRY);
		Log.log(Log.DEBUG, this, "DONE");
	}

	private void saveGeometry()
	{
		GUIUtilities.saveGeometry(this, GEOMETRY);
	}
	
	private String[] getProtocNames(String protocPropName) {
        List<ProtoCompiler> protoCompilers = readCompilers(jEdit.getProperty("options.protobuf.protoc.compilers", ""));
        String[] names = new String[protoCompilers.size()];
        for (int i=0;i<names.length;i++) {
        	names[i] = protoCompilers.get(i).getName();
        }
        
        return names;
	}

	private JTextArea makeTextBox(String initialValue) {
		JTextArea textBox = new JTextArea(initialValue,2,60);
		textBox.setEditable(true);
		textBox.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		textBox.setSize(2,60);
		textBox.setLineWrap(true);
		return textBox;
	}

	private JTextArea makeDisplayBox() {
		JTextArea textBox = new JTextArea("",10,60);
		textBox.setEditable(false);
		textBox.setBorder(BorderFactory.createLineBorder(Color.GRAY,1));
		textBox.setSize(10,60);
		textBox.setLineWrap(true);
		return textBox;
	}
	
	private String getProtocExecutable(String name) {
        List<ProtoCompiler> protoCompilers = readCompilers(jEdit.getProperty("options.protobuf.protoc.compilers", ""));
        String path = "";
        for (ProtoCompiler compiler : protoCompilers) {
        	if (name.equals(compiler.getName())) {
        		path = compiler.getPath();
        		break;
        	}
        }      
        return path;
	}
}
