// Generated from ProtoParser.g4 by ANTLR 4.x
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ProtoParserParser}.
 */
public interface ProtoParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#all_identifier}.
	 * @param ctx the parse tree
	 */
	void enterAll_identifier(@NotNull ProtoParserParser.All_identifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#all_identifier}.
	 * @param ctx the parse tree
	 */
	void exitAll_identifier(@NotNull ProtoParserParser.All_identifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#all_value}.
	 * @param ctx the parse tree
	 */
	void enterAll_value(@NotNull ProtoParserParser.All_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#all_value}.
	 * @param ctx the parse tree
	 */
	void exitAll_value(@NotNull ProtoParserParser.All_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value(@NotNull ProtoParserParser.Literal_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value(@NotNull ProtoParserParser.Literal_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#proto_type}.
	 * @param ctx the parse tree
	 */
	void enterProto_type(@NotNull ProtoParserParser.Proto_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#proto_type}.
	 * @param ctx the parse tree
	 */
	void exitProto_type(@NotNull ProtoParserParser.Proto_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#proto}.
	 * @param ctx the parse tree
	 */
	void enterProto(@NotNull ProtoParserParser.ProtoContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#proto}.
	 * @param ctx the parse tree
	 */
	void exitProto(@NotNull ProtoParserParser.ProtoContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#package_def}.
	 * @param ctx the parse tree
	 */
	void enterPackage_def(@NotNull ProtoParserParser.Package_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#package_def}.
	 * @param ctx the parse tree
	 */
	void exitPackage_def(@NotNull ProtoParserParser.Package_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#package_name}.
	 * @param ctx the parse tree
	 */
	void enterPackage_name(@NotNull ProtoParserParser.Package_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#package_name}.
	 * @param ctx the parse tree
	 */
	void exitPackage_name(@NotNull ProtoParserParser.Package_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#import_def}.
	 * @param ctx the parse tree
	 */
	void enterImport_def(@NotNull ProtoParserParser.Import_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#import_def}.
	 * @param ctx the parse tree
	 */
	void exitImport_def(@NotNull ProtoParserParser.Import_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#import_file_name}.
	 * @param ctx the parse tree
	 */
	void enterImport_file_name(@NotNull ProtoParserParser.Import_file_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#import_file_name}.
	 * @param ctx the parse tree
	 */
	void exitImport_file_name(@NotNull ProtoParserParser.Import_file_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#syntax_def}.
	 * @param ctx the parse tree
	 */
	void enterSyntax_def(@NotNull ProtoParserParser.Syntax_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#syntax_def}.
	 * @param ctx the parse tree
	 */
	void exitSyntax_def(@NotNull ProtoParserParser.Syntax_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#syntax_name}.
	 * @param ctx the parse tree
	 */
	void enterSyntax_name(@NotNull ProtoParserParser.Syntax_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#syntax_name}.
	 * @param ctx the parse tree
	 */
	void exitSyntax_name(@NotNull ProtoParserParser.Syntax_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#option_line_def}.
	 * @param ctx the parse tree
	 */
	void enterOption_line_def(@NotNull ProtoParserParser.Option_line_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#option_line_def}.
	 * @param ctx the parse tree
	 */
	void exitOption_line_def(@NotNull ProtoParserParser.Option_line_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#option_field_def}.
	 * @param ctx the parse tree
	 */
	void enterOption_field_def(@NotNull ProtoParserParser.Option_field_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#option_field_def}.
	 * @param ctx the parse tree
	 */
	void exitOption_field_def(@NotNull ProtoParserParser.Option_field_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#option_field_item}.
	 * @param ctx the parse tree
	 */
	void enterOption_field_item(@NotNull ProtoParserParser.Option_field_itemContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#option_field_item}.
	 * @param ctx the parse tree
	 */
	void exitOption_field_item(@NotNull ProtoParserParser.Option_field_itemContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#option_all_value}.
	 * @param ctx the parse tree
	 */
	void enterOption_all_value(@NotNull ProtoParserParser.Option_all_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#option_all_value}.
	 * @param ctx the parse tree
	 */
	void exitOption_all_value(@NotNull ProtoParserParser.Option_all_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#option_value_object}.
	 * @param ctx the parse tree
	 */
	void enterOption_value_object(@NotNull ProtoParserParser.Option_value_objectContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#option_value_object}.
	 * @param ctx the parse tree
	 */
	void exitOption_value_object(@NotNull ProtoParserParser.Option_value_objectContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#option_value_item}.
	 * @param ctx the parse tree
	 */
	void enterOption_value_item(@NotNull ProtoParserParser.Option_value_itemContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#option_value_item}.
	 * @param ctx the parse tree
	 */
	void exitOption_value_item(@NotNull ProtoParserParser.Option_value_itemContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#option_name}.
	 * @param ctx the parse tree
	 */
	void enterOption_name(@NotNull ProtoParserParser.Option_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#option_name}.
	 * @param ctx the parse tree
	 */
	void exitOption_name(@NotNull ProtoParserParser.Option_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#enum_def}.
	 * @param ctx the parse tree
	 */
	void enterEnum_def(@NotNull ProtoParserParser.Enum_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#enum_def}.
	 * @param ctx the parse tree
	 */
	void exitEnum_def(@NotNull ProtoParserParser.Enum_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#enum_name}.
	 * @param ctx the parse tree
	 */
	void enterEnum_name(@NotNull ProtoParserParser.Enum_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#enum_name}.
	 * @param ctx the parse tree
	 */
	void exitEnum_name(@NotNull ProtoParserParser.Enum_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#enum_content}.
	 * @param ctx the parse tree
	 */
	void enterEnum_content(@NotNull ProtoParserParser.Enum_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#enum_content}.
	 * @param ctx the parse tree
	 */
	void exitEnum_content(@NotNull ProtoParserParser.Enum_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#enum_item_def}.
	 * @param ctx the parse tree
	 */
	void enterEnum_item_def(@NotNull ProtoParserParser.Enum_item_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#enum_item_def}.
	 * @param ctx the parse tree
	 */
	void exitEnum_item_def(@NotNull ProtoParserParser.Enum_item_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#message_def}.
	 * @param ctx the parse tree
	 */
	void enterMessage_def(@NotNull ProtoParserParser.Message_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#message_def}.
	 * @param ctx the parse tree
	 */
	void exitMessage_def(@NotNull ProtoParserParser.Message_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#message_name}.
	 * @param ctx the parse tree
	 */
	void enterMessage_name(@NotNull ProtoParserParser.Message_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#message_name}.
	 * @param ctx the parse tree
	 */
	void exitMessage_name(@NotNull ProtoParserParser.Message_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#message_content}.
	 * @param ctx the parse tree
	 */
	void enterMessage_content(@NotNull ProtoParserParser.Message_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#message_content}.
	 * @param ctx the parse tree
	 */
	void exitMessage_content(@NotNull ProtoParserParser.Message_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#message_item_def}.
	 * @param ctx the parse tree
	 */
	void enterMessage_item_def(@NotNull ProtoParserParser.Message_item_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#message_item_def}.
	 * @param ctx the parse tree
	 */
	void exitMessage_item_def(@NotNull ProtoParserParser.Message_item_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#message_ext_def}.
	 * @param ctx the parse tree
	 */
	void enterMessage_ext_def(@NotNull ProtoParserParser.Message_ext_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#message_ext_def}.
	 * @param ctx the parse tree
	 */
	void exitMessage_ext_def(@NotNull ProtoParserParser.Message_ext_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#ext_def}.
	 * @param ctx the parse tree
	 */
	void enterExt_def(@NotNull ProtoParserParser.Ext_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#ext_def}.
	 * @param ctx the parse tree
	 */
	void exitExt_def(@NotNull ProtoParserParser.Ext_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#ext_name}.
	 * @param ctx the parse tree
	 */
	void enterExt_name(@NotNull ProtoParserParser.Ext_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#ext_name}.
	 * @param ctx the parse tree
	 */
	void exitExt_name(@NotNull ProtoParserParser.Ext_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#ext_content}.
	 * @param ctx the parse tree
	 */
	void enterExt_content(@NotNull ProtoParserParser.Ext_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#ext_content}.
	 * @param ctx the parse tree
	 */
	void exitExt_content(@NotNull ProtoParserParser.Ext_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#service_def}.
	 * @param ctx the parse tree
	 */
	void enterService_def(@NotNull ProtoParserParser.Service_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#service_def}.
	 * @param ctx the parse tree
	 */
	void exitService_def(@NotNull ProtoParserParser.Service_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#service_name}.
	 * @param ctx the parse tree
	 */
	void enterService_name(@NotNull ProtoParserParser.Service_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#service_name}.
	 * @param ctx the parse tree
	 */
	void exitService_name(@NotNull ProtoParserParser.Service_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#service_content}.
	 * @param ctx the parse tree
	 */
	void enterService_content(@NotNull ProtoParserParser.Service_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#service_content}.
	 * @param ctx the parse tree
	 */
	void exitService_content(@NotNull ProtoParserParser.Service_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#rpc_def}.
	 * @param ctx the parse tree
	 */
	void enterRpc_def(@NotNull ProtoParserParser.Rpc_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#rpc_def}.
	 * @param ctx the parse tree
	 */
	void exitRpc_def(@NotNull ProtoParserParser.Rpc_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#rpc_name}.
	 * @param ctx the parse tree
	 */
	void enterRpc_name(@NotNull ProtoParserParser.Rpc_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#rpc_name}.
	 * @param ctx the parse tree
	 */
	void exitRpc_name(@NotNull ProtoParserParser.Rpc_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#req_name}.
	 * @param ctx the parse tree
	 */
	void enterReq_name(@NotNull ProtoParserParser.Req_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#req_name}.
	 * @param ctx the parse tree
	 */
	void exitReq_name(@NotNull ProtoParserParser.Req_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link ProtoParserParser#resp_name}.
	 * @param ctx the parse tree
	 */
	void enterResp_name(@NotNull ProtoParserParser.Resp_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ProtoParserParser#resp_name}.
	 * @param ctx the parse tree
	 */
	void exitResp_name(@NotNull ProtoParserParser.Resp_nameContext ctx);
}