package protobuf.options;

public class ProtoCompiler {
	
	private String name;
	private String path;
	private boolean removable;
	
	public ProtoCompiler (String name, String path) {
		this.name = name;
		this.path = path;
		this.removable = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}
	
	public boolean isRemovable() {
		return this.removable;
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
}
