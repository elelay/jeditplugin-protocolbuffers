package protobuf.options;

import java.util.ArrayList;
import java.util.List;

public class OptionsUtils {

    public static List<ProtoCompiler> readCompilers(String compilerList) {

    	List<ProtoCompiler> compilersList = new ArrayList<>();

    	String[] compilers = compilerList.split(",");
    	for (int i = 0;i < compilers.length;i++) {
    		if (compilers[i].contains(";")) {
    			String[] compilerCols = compilers[i].split(";");
    			ProtoCompiler compiler = new ProtoCompiler(compilerCols[0], compilerCols[1]);
    			compilersList.add(compiler);
    		}
    	}    	
    	return compilersList;
    }

    public static String writeCompilers(List<ProtoCompiler> compilersList) {

    	StringBuilder compilers = new StringBuilder();
    	boolean firstTime = true;
    	
    	for (ProtoCompiler compiler : compilersList) {
    		if (!firstTime){
        		compilers.append(",");
    		} else {
    			firstTime = false;
    		}
    		compilers.append(compiler.getName());
    		compilers.append(";");
    		compilers.append(compiler.getPath());		
    	}    	
    	return compilers.toString();
    }

	
}
