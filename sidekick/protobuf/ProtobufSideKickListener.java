package sidekick.protobuf;

import java.util.ArrayDeque;
import java.util.Deque;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.text.Position;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;

import eclipseicons.EclipseIconsPlugin;
import sidekick.protobuf.parser.ProtoParserBaseListener;
import sidekick.protobuf.parser.ProtoParserParser;
import sidekick.util.Location;
import sidekick.util.SideKickPosition;

public class ProtobufSideKickListener extends ProtoParserBaseListener {

    ProtobufNode root = null;
    final String fileName;
    final Deque<ProtobufNode> stack = new ArrayDeque<ProtobufNode>();
    
    final ImageIcon serviceIcon = EclipseIconsPlugin.getIcon("searchm_obj.gif");
    final ImageIcon messageIcon = EclipseIconsPlugin.getIcon("methpub_obj.gif");
    final ImageIcon enumIcon = EclipseIconsPlugin.getIcon("enum_obj.gif");
    final ImageIcon repeatedIcon = EclipseIconsPlugin.getIcon("recursive_co.gif");
    final ImageIcon requiredIcon = EclipseIconsPlugin.getIcon("hprio_tsk.gif");//add.gif, lock.gif
    final ImageIcon optionalIcon = EclipseIconsPlugin.getIcon("question_ov.gif");
 
    public ProtobufSideKickListener(String fileName) {
    	this.fileName = fileName;
    }
	
    public ProtobufNode getRoot() {
        return root;
    }

    public String getFileName() {
        return fileName;
    }

    
    @Override public void enterProto( @NotNull ProtoParserParser.ProtoContext ctx ) {
        root = new ProtobufNode(getFileName());
        root.setStartLocation( getStartLocation( ctx ) );
        root.setEndLocation( getEndLocation( ctx ) );
        stack.push( root );
    }
    
    @Override public void exitProto( @NotNull ProtoParserParser.ProtoContext ctx ) {
        stack.pop();    // stack should be empty at this point
        ProtobufNode child = root.getFirstChild();
        Position start;
        Position end;
        if (child != null) {
            start = child.getStartPosition();
            end = child.getEndPosition();
        } else {
            start = new SideKickPosition(0);
            end = start;
        }
        root.setStartPosition(start);
        root.setEndPosition(end);
    }

    
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMessage_def(@NotNull ProtoParserParser.Message_defContext ctx) { 
		ProtobufNode node = new ProtobufNode("function");
        node.setIcon(messageIcon);
        node.setStartLocation( getStartLocation( ctx ) );
        node.setEndLocation( getEndLocation( ctx ) );
        
        String name = ctx.message_name().IDENTIFIER().toString();// ctx.name.getChild(0).toString();
        node.setName(name);

        node.setStartPosition(new SideKickPosition(ctx.getStart().getStartIndex()));
        node.setEndPosition(new SideKickPosition(ctx.getStop().getStartIndex()));
       
        stack.push(node);        
	}

	@Override public void exitMessage_def(@NotNull ProtoParserParser.Message_defContext ctx) { 
		ProtobufNode node = stack.pop();
		ProtobufNode parent = stack.peek();
        parent.addChild(node);        
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterMessage_item_def(@NotNull ProtoParserParser.Message_item_defContext ctx) { 
		
		ProtobufNode node = new ProtobufNode("function");
		
		String scope = ctx.PROTOBUF_SCOPE_LITERAL().toString();
		ImageIcon nodeIcon = optionalIcon;
		if ("repeated".equals(scope)) {
			nodeIcon = repeatedIcon;
		} else if ("required".equals(scope)) {
			nodeIcon = requiredIcon;
		}
		
        node.setIcon(nodeIcon);
        node.setStartLocation( getStartLocation( ctx ) );
        node.setEndLocation( getEndLocation( ctx ) );
        
        String name = ctx.IDENTIFIER().toString();// ctx.name.getChild(0).toString();
        node.setName(name);

        node.setStartPosition(new SideKickPosition(ctx.getStart().getStartIndex()));
        node.setEndPosition(new SideKickPosition(ctx.getStop().getStartIndex()));
       
        stack.push(node);        
		
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitMessage_item_def(@NotNull ProtoParserParser.Message_item_defContext ctx) { 
		ProtobufNode node = stack.pop();
		ProtobufNode parent = stack.peek();
        parent.addChild(node);        		
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterService_def(@NotNull ProtoParserParser.Service_defContext ctx) { 
		ProtobufNode node = new ProtobufNode("function");
        node.setIcon(serviceIcon);
        node.setStartLocation( getStartLocation( ctx ) );
        node.setEndLocation( getEndLocation( ctx ) );
        
        String name = ctx.service_name().IDENTIFIER().toString();// ctx.name.getChild(0).toString();
        node.setName(name);

        node.setStartPosition(new SideKickPosition(ctx.getStart().getStartIndex()));
        node.setEndPosition(new SideKickPosition(ctx.getStop().getStartIndex()));
       
        stack.push(node);        
	}

	@Override public void exitService_def(@NotNull ProtoParserParser.Service_defContext ctx) { 

		ProtobufNode node = stack.pop();
		ProtobufNode parent = stack.peek();
        parent.addChild(node);        
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEnum_def(@NotNull ProtoParserParser.Enum_defContext ctx) { 

        ProtobufNode node = new ProtobufNode("function");
        node.setIcon(enumIcon);
        node.setStartLocation( getStartLocation( ctx ) );
        node.setEndLocation( getEndLocation( ctx ) );
        
        String name = ctx.enum_name().IDENTIFIER().toString();// ctx.name.getChild(0).toString();
        node.setName(name);

        node.setStartPosition(new SideKickPosition(ctx.getStart().getStartIndex()));
        node.setEndPosition(new SideKickPosition(ctx.getStop().getStartIndex()));
       
        stack.push(node);        
	}

	/* (non-Javadoc)
	 * @see sidekick.protobuf.parser.ProtoParserBaseListener#exitEnum_def(sidekick.protobuf.parser.ProtoParserParser.Enum_defContext)
	 */
	@Override public void exitEnum_def(@NotNull ProtoParserParser.Enum_defContext ctx) { 

		ProtobufNode node = stack.pop();
		ProtobufNode parent = stack.peek();
        parent.addChild(node);        
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEnum_item_def(@NotNull ProtoParserParser.Enum_item_defContext ctx) { 

		ProtobufNode node = new ProtobufNode("function");
        node.setIcon(messageIcon);
        node.setStartLocation( getStartLocation( ctx ) );
        node.setEndLocation( getEndLocation( ctx ) );

        String name = ctx.IDENTIFIER().toString();// ctx.name.getChild(0).toString();
        node.setName(name);

        node.setStartPosition(new SideKickPosition(ctx.getStart().getStartIndex()));
        node.setEndPosition(new SideKickPosition(ctx.getStop().getStartIndex()));
       
        stack.push(node);        
		
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEnum_item_def(@NotNull ProtoParserParser.Enum_item_defContext ctx) { 
		
		ProtobufNode node = stack.pop();
		ProtobufNode parent = stack.peek();
        parent.addChild(node);        
		
	}
	
	
    // return a Location representing the start of the rule context
    private Location getStartLocation( ParserRuleContext ctx ) {
        int line = ctx.getStart().getLine();
        int col = ctx.getStart().getCharPositionInLine();
        return new Location( line, col );
    }

    // return a Location representing the end of the rule context
    private Location getEndLocation( ParserRuleContext ctx ) {
        int line = ctx.getStop().getLine();
        int col = ctx.getStop().getCharPositionInLine();
        return new Location( line, col );
    }

}
