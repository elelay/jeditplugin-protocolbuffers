package protobuf.options;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ProtoCompilerTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 8291890846564957821L;
	private final List<ProtoCompiler> compilers;

    private final String[] columnNames = new String[] {
            "Name", "Path", "Remove"
    };
    
    @SuppressWarnings("rawtypes")
	private final Class[] columnClass = new Class[] {
        String.class, String.class, Boolean.class
    };
	
    public ProtoCompilerTableModel (List<ProtoCompiler> compilers) {
    	this.compilers = compilers;
    }
    
	public void addCompiler(String name, String path) {
		ProtoCompiler compiler = new ProtoCompiler(name,path);
		compilers.add(compiler);
		this.fireTableDataChanged();
	}

	public void removeCompiler(String name) {
		int removeIndex = 0;
		boolean nameFound = false;
		
		for (int i = 0; i < compilers.size(); i++) {
			ProtoCompiler compiler = compilers.get(i);
			if (compiler.getName().equals(name)) {
				removeIndex = i;
				nameFound = true;
				break;
			}
		}
		
		if (nameFound) {
			compilers.remove(removeIndex);
			this.fireTableDataChanged();
		}
	}

	public void removeSelected() {		
		for (Iterator<ProtoCompiler> it = compilers.iterator(); it.hasNext();){
			ProtoCompiler compiler = it.next();
			if (compiler.isRemovable()) {
				it.remove();
			}
		}
		this.fireTableDataChanged();
	}

	public List<ProtoCompiler> getCompilers() {
		return compilers;
	}
	
	@Override
	public int getRowCount() {
		return compilers.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
	    return true;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		ProtoCompiler compilerRow = compilers.get(rowIndex);
		if (0 == columnIndex) {
			return compilerRow.getName();
		} else if (1 == columnIndex) {
			return compilerRow.getPath();	
		} else if (2 == columnIndex) {
			return compilerRow.isRemovable();	
		} else {
			return null;
		}	
	}
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		ProtoCompiler compilerRow = compilers.get(rowIndex);
		if (0 == columnIndex) {
			compilerRow.setName(String.valueOf(value));
		} else if (1 == columnIndex) {
			compilerRow.setPath(String.valueOf(value));	
		} else if (2 == columnIndex) {
			compilerRow.setRemovable((Boolean)value);	
		} 
	}

}
