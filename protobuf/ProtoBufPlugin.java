package protobuf;

import org.gjt.sp.jedit.Buffer;
import org.gjt.sp.jedit.EditPlugin;
import org.gjt.sp.jedit.jEdit;

/**
 * @author Tim Blackler
 *
 */
public class ProtoBufPlugin extends EditPlugin {
	public static final String NAME = "protobuf";
    
    public void start() {
    }

    public void stop() {
    }

    public static void generateFiles() {
    	GenerateProtobufDialog dlg = new GenerateProtobufDialog(jEdit.getActiveView());
		dlg.setVisible(true);
    }

    public static void generateFiles(Buffer buffer) {
        if ("protobuf".equals(buffer.getMode().getName())) {
        	GenerateProtobufDialog dlg = new GenerateProtobufDialog(jEdit.getActiveView(), buffer.getDirectory(), buffer.getName());
        	dlg.setVisible(true);
        }   	
    }

}	