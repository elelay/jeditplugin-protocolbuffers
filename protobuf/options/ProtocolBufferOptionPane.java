/*
 * ProtocolBufferOptionPane.java - options panel for ProtocolBufferPlugin
 * Copyright (C) 2016 Tim Blackler
 *
 * jEdit edit mode settings:
 * :mode=java:tabSize=4:indentSize=4:noTabs=true:maxLineLen=0:
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package protobuf.options;


import static protobuf.options.OptionsUtils.readCompilers;
import static protobuf.options.OptionsUtils.writeCompilers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import org.gjt.sp.jedit.AbstractOptionPane;
import org.gjt.sp.jedit.jEdit;

import protobuf.FileSelectionPanel;


/**
 * An option panel for the Protocol Buffer plugin.
 *
 * @author Tim Blackler
 */
public class ProtocolBufferOptionPane extends AbstractOptionPane
{

	private static final long serialVersionUID = 5947532463024083203L;
	
	public ProtocolBufferOptionPane() {
        super("protobuf");
    }


    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    public void _init() {
    	this.setLayout(new BorderLayout());		
        JPanel panel = new JPanel(new BorderLayout());         

    	JLabel heading = new JLabel(jEdit.getProperty("options.protobuf.protoc.compilers.headings"));
        panel.add(BorderLayout.NORTH,heading);
   
        List<ProtoCompiler> protoCompilers = readCompilers(jEdit.getProperty("options.protobuf.protoc.compilers", ""));

        //create the model
        model = new ProtoCompilerTableModel(protoCompilers);
        
        //create the table
        JTable table = new JTable(model);
         
        //add the table to the frame
        JScrollPane tablePane = new JScrollPane(table);
	    Dimension tablePaneSize = tablePane.getPreferredSize();
	    tablePane.setPreferredSize(new Dimension(tablePaneSize.width,table.getRowHeight()*10));
        panel.add(BorderLayout.CENTER,tablePane);
        
        JPanel editPanel = new JPanel(new GridLayout(2,1));
        panel.add(BorderLayout.SOUTH,editPanel);
        
        JPanel addPanel = new JPanel();
        addPanel.add(new JLabel("Name:"));
        name = new JTextArea("",1,20);
        name.setEditable(true);
        name.setBorder(BorderFactory.createLineBorder(Color.GRAY,1));

        addPanel.add(name);
        addPanel.add(new JLabel("Path:"));
        path = new FileSelectionPanel("protobuf.selectProtocOptionFile", 20);
        path.setSelectionEnabled(true);
        path.setSourceFieldText("");
        
        addPanel.add(path);
        editPanel.add(addPanel);

        //Create the button panel
        JPanel buttonPanel = new JPanel();
        JButton addButton = new JButton("Add");
        
        addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.addCompiler(name.getText(), path.getSourceFieldText());
				name.setText("");
				path.setSourceFieldText("");
			}
        	
        });
        
        JButton removeButton = new JButton("Remove");
        removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.removeSelected();
			}
        	
        });

        buttonPanel.add(addButton);
        
        buttonPanel.add(removeButton);
        editPanel.add(buttonPanel);
        
        addComponent(panel);
    }

    /**
     * Called when the options dialog's `OK' button is pressed.
     * This saves any properties saved in this option pane.
     */
    @Override
    public void _save() {
    	List<ProtoCompiler> protoCompilers = model.getCompilers();    	
    	jEdit.setProperty("options.protobuf.protoc.compilers", writeCompilers(protoCompilers));      
    }

    
    // Compiler types
	private ProtoCompilerTableModel model;
	private JTextArea name;
	private FileSelectionPanel path;

}
