grammar ProtoParser;
import ProtoLexer;


// Predefines
all_identifier
  :  IDENTIFIER
  |  QUALIFIED_IDENTIFIER
  ;

all_value
  :  IDENTIFIER
  |  literal_value
  ;

literal_value
  :  INTEGER_LITERAL
  |  STRING_LITERAL
  |  BOOL_LITERAL
  |  FLOAT_LITERAL
  ;

proto_type
  :  PROTOBUF_TYPE_LITERAL
  |  all_identifier
  ;

// Proto ------------------------------
proto
  :  (package_def | import_def | syntax_def | option_line_def | enum_def | ext_def | message_def | service_def)* EOF  // Only one package define is allowed - will handle that in the compiler
  ;
// Proto ------------------------------

// Package ----------------------------
package_def
  :  PACKAGE_LITERAL package_name ITEM_TERMINATOR
  ;

package_name : all_identifier ;
// Package ----------------------------

// Import -----------------------------
import_def
  :  IMPORT_LITERAL import_file_name ITEM_TERMINATOR
  ;

import_file_name : STRING_LITERAL ;
// Import -----------------------------

// Syntax -----------------------------
syntax_def
  :  SYNTAX_LITERAL EQUALS syntax_name ITEM_TERMINATOR
  ;

syntax_name : STRING_LITERAL ;
// Syntax -----------------------------

// Option in line----------------------
option_line_def
  :  OPTION_LITERAL option_name EQUALS option_all_value ITEM_TERMINATOR
  ;

option_field_def
  :  BRACKET_OPEN option_field_item (COMMA option_field_item)* BRACKET_CLOSE
  ;

option_field_item
  :  option_name EQUALS option_all_value
  ;

option_all_value
  : all_value
  | option_value_object
  ;

option_value_object
  :  BLOCK_OPEN option_value_item* BLOCK_CLOSE
  ;

option_value_item
  :  IDENTIFIER COLON option_all_value
  ;

option_name
  :  IDENTIFIER
  |  PAREN_OPEN all_identifier PAREN_CLOSE FIELD_IDENTIFIER*
  ;
// Option in line----------------------

// Enum -------------------------------
enum_def
  :  ENUM_LITERAL enum_name BLOCK_OPEN enum_content BLOCK_CLOSE
  ;

enum_name : IDENTIFIER ;

enum_content : (option_line_def | enum_item_def)* ;

enum_item_def
  :  IDENTIFIER EQUALS INTEGER_LITERAL option_field_def? ITEM_TERMINATOR
  ;
// Enum -------------------------------

// Message ----------------------------
message_def
  :  MESSAGE_LITERAL message_name BLOCK_OPEN message_content? BLOCK_CLOSE
  ;

message_name : IDENTIFIER ;

message_content : (option_line_def | message_item_def | message_def | enum_def | message_ext_def)+ ;

message_item_def
  : PROTOBUF_SCOPE_LITERAL proto_type IDENTIFIER EQUALS INTEGER_LITERAL option_field_def? ITEM_TERMINATOR
  ;

message_ext_def
  : EXTENSIONS_DEF_LITERAL INTEGER_LITERAL EXTENSIONS_TO_LITERAL (v=INTEGER_LITERAL | v=EXTENSIONS_MAX_LITERAL) ITEM_TERMINATOR
  ;
// Message ----------------------------

// Extension --------------------------
ext_def
  :  EXTEND_LITERAL ext_name BLOCK_OPEN ext_content? BLOCK_CLOSE
  ;

ext_name : all_identifier ;

ext_content : (option_line_def | message_item_def | message_def | enum_def)+ ;
// Extension --------------------------

// Service ----------------------------
service_def
  :  SERVICE_LITERAL service_name BLOCK_OPEN service_content? BLOCK_CLOSE
  ;

service_name : IDENTIFIER ;

service_content : (option_line_def | rpc_def )+ ;

rpc_def
  :  RPC_LITERAL rpc_name PAREN_OPEN req_name PAREN_CLOSE RETURNS_LITERAL PAREN_OPEN resp_name PAREN_CLOSE (BLOCK_OPEN option_line_def* BLOCK_CLOSE ITEM_TERMINATOR? | ITEM_TERMINATOR)
  ;

rpc_name : IDENTIFIER ;
req_name : all_identifier ;
resp_name : all_identifier ;
// Service ----------------------------
