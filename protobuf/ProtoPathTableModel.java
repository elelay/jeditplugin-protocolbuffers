package protobuf;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ProtoPathTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 8291890846564957821L;
	private final List<String> paths;

    private final String[] columnNames = new String[] {
            "Path"
    };
    
    @SuppressWarnings("rawtypes")
	private final Class[] columnClass = new Class[] {
        String.class
    };

    public ProtoPathTableModel () {
    	this(new ArrayList<String>());
    }

    public ProtoPathTableModel (String initial) {
    	this();
    	paths.add(initial);    	
    }

    
    public ProtoPathTableModel (List<String> paths) {
    	this.paths = paths;
    }
    
	public void addPath(String path) {
		paths.add(path);
		this.fireTableDataChanged();
	}

	public void removePath(int removeIndex) {
		paths.remove(removeIndex);
		this.fireTableDataChanged();
	}

	public void removePath(String name) {
		int removeIndex = 0;
		boolean nameFound = false;
		
		for (int i = 0; i < paths.size(); i++) {
			String path = paths.get(i);
			if (path.equals(name)) {
				removeIndex = i;
				nameFound = true;
				break;
			}
		}
		
		if (nameFound) {
			paths.remove(removeIndex);
			this.fireTableDataChanged();
		}
	}

	public void clear() {
		paths.clear();
		this.fireTableDataChanged();
	}

	
	public List<String> getPaths() {
		return paths;
	}
	
	@Override
	public int getRowCount() {
		return paths.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	
    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnClass[columnIndex];
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
	    return true;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String path = paths.get(rowIndex);
		if (0 == columnIndex) {
			return path;
		} else {
			return null;
		}	
	}
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		paths.set(rowIndex, (String)value);		
	}

}
