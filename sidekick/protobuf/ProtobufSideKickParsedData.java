
package sidekick.protobuf;

// Imports
import sidekick.SideKickParsedData;

/**
 * Stores a buffer structure tree.
 */
public class ProtobufSideKickParsedData extends SideKickParsedData {
    /**
     * @param fileName The file name being parsed, used as the root of the
     * tree.
     */
    public ProtobufSideKickParsedData( String fileName ) {
        super( fileName );
    }
}
