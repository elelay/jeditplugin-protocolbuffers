// Generated from ProtoParser.g4 by ANTLR 4.x
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

public class ProtoParserParser extends Parser {
	public static final int
		PACKAGE_LITERAL=1, IMPORT_LITERAL=2, OPTION_LITERAL=3, SYNTAX_LITERAL=4, 
		ENUM_LITERAL=5, MESSAGE_LITERAL=6, EXTEND_LITERAL=7, EXTENSIONS_DEF_LITERAL=8, 
		EXTENSIONS_TO_LITERAL=9, EXTENSIONS_MAX_LITERAL=10, SERVICE_LITERAL=11, 
		RETURNS_LITERAL=12, RPC_LITERAL=13, BLOCK_OPEN=14, BLOCK_CLOSE=15, PAREN_OPEN=16, 
		PAREN_CLOSE=17, BRACKET_OPEN=18, BRACKET_CLOSE=19, EQUALS=20, COLON=21, 
		COMMA=22, ITEM_TERMINATOR=23, PROTOBUF_SCOPE_LITERAL=24, PROTOBUF_TYPE_LITERAL=25, 
		INTEGER_LITERAL=26, STRING_LITERAL=27, BOOL_LITERAL=28, FLOAT_LITERAL=29, 
		IDENTIFIER=30, QUALIFIED_IDENTIFIER=31, FIELD_IDENTIFIER=32, COMMENT=33, 
		LINE_COMMENT=34, WHITESPACE=35;
	public static final int
		RULE_all_identifier = 0, RULE_all_value = 1, RULE_literal_value = 2, RULE_proto_type = 3, 
		RULE_proto = 4, RULE_package_def = 5, RULE_package_name = 6, RULE_import_def = 7, 
		RULE_import_file_name = 8, RULE_syntax_def = 9, RULE_syntax_name = 10, 
		RULE_option_line_def = 11, RULE_option_field_def = 12, RULE_option_field_item = 13, 
		RULE_option_all_value = 14, RULE_option_value_object = 15, RULE_option_value_item = 16, 
		RULE_option_name = 17, RULE_enum_def = 18, RULE_enum_name = 19, RULE_enum_content = 20, 
		RULE_enum_item_def = 21, RULE_message_def = 22, RULE_message_name = 23, 
		RULE_message_content = 24, RULE_message_item_def = 25, RULE_message_ext_def = 26, 
		RULE_ext_def = 27, RULE_ext_name = 28, RULE_ext_content = 29, RULE_service_def = 30, 
		RULE_service_name = 31, RULE_service_content = 32, RULE_rpc_def = 33, 
		RULE_rpc_name = 34, RULE_req_name = 35, RULE_resp_name = 36;
	public static final String[] ruleNames = {
		"all_identifier", "all_value", "literal_value", "proto_type", "proto", 
		"package_def", "package_name", "import_def", "import_file_name", "syntax_def", 
		"syntax_name", "option_line_def", "option_field_def", "option_field_item", 
		"option_all_value", "option_value_object", "option_value_item", "option_name", 
		"enum_def", "enum_name", "enum_content", "enum_item_def", "message_def", 
		"message_name", "message_content", "message_item_def", "message_ext_def", 
		"ext_def", "ext_name", "ext_content", "service_def", "service_name", "service_content", 
		"rpc_def", "rpc_name", "req_name", "resp_name"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'package'", "'import'", "'option'", "'syntax'", "'enum'", "'message'", 
		"'extend'", "'extensions'", "'to'", "'max'", "'service'", "'returns'", 
		"'rpc'", "'{'", "'}'", "'('", "')'", "'['", "']'", "'='", "':'", "','", 
		"';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "PACKAGE_LITERAL", "IMPORT_LITERAL", "OPTION_LITERAL", "SYNTAX_LITERAL", 
		"ENUM_LITERAL", "MESSAGE_LITERAL", "EXTEND_LITERAL", "EXTENSIONS_DEF_LITERAL", 
		"EXTENSIONS_TO_LITERAL", "EXTENSIONS_MAX_LITERAL", "SERVICE_LITERAL", 
		"RETURNS_LITERAL", "RPC_LITERAL", "BLOCK_OPEN", "BLOCK_CLOSE", "PAREN_OPEN", 
		"PAREN_CLOSE", "BRACKET_OPEN", "BRACKET_CLOSE", "EQUALS", "COLON", "COMMA", 
		"ITEM_TERMINATOR", "PROTOBUF_SCOPE_LITERAL", "PROTOBUF_TYPE_LITERAL", 
		"INTEGER_LITERAL", "STRING_LITERAL", "BOOL_LITERAL", "FLOAT_LITERAL", 
		"IDENTIFIER", "QUALIFIED_IDENTIFIER", "FIELD_IDENTIFIER", "COMMENT", "LINE_COMMENT", 
		"WHITESPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override
	@NotNull
	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "ProtoParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	public ProtoParserParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN);
	}
	public static class All_identifierContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public TerminalNode QUALIFIED_IDENTIFIER() { return getToken(ProtoParserParser.QUALIFIED_IDENTIFIER, 0); }
		public All_identifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_all_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterAll_identifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitAll_identifier(this);
		}
	}

	@RuleVersion(0)
	public final All_identifierContext all_identifier() throws RecognitionException {
		All_identifierContext _localctx = new All_identifierContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_all_identifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			_la = _input.LA(1);
			if ( !(_la==IDENTIFIER || _la==QUALIFIED_IDENTIFIER) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class All_valueContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public All_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_all_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterAll_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitAll_value(this);
		}
	}

	@RuleVersion(0)
	public final All_valueContext all_value() throws RecognitionException {
		All_valueContext _localctx = new All_valueContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_all_value);
		try {
			setState(78);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(76);
				match(IDENTIFIER);
				}
				break;
			case INTEGER_LITERAL:
			case STRING_LITERAL:
			case BOOL_LITERAL:
			case FLOAT_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(77);
				literal_value();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Literal_valueContext extends ParserRuleContext {
		public TerminalNode INTEGER_LITERAL() { return getToken(ProtoParserParser.INTEGER_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(ProtoParserParser.STRING_LITERAL, 0); }
		public TerminalNode BOOL_LITERAL() { return getToken(ProtoParserParser.BOOL_LITERAL, 0); }
		public TerminalNode FLOAT_LITERAL() { return getToken(ProtoParserParser.FLOAT_LITERAL, 0); }
		public Literal_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterLiteral_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitLiteral_value(this);
		}
	}

	@RuleVersion(0)
	public final Literal_valueContext literal_value() throws RecognitionException {
		Literal_valueContext _localctx = new Literal_valueContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_literal_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INTEGER_LITERAL) | (1L << STRING_LITERAL) | (1L << BOOL_LITERAL) | (1L << FLOAT_LITERAL))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Proto_typeContext extends ParserRuleContext {
		public TerminalNode PROTOBUF_TYPE_LITERAL() { return getToken(ProtoParserParser.PROTOBUF_TYPE_LITERAL, 0); }
		public All_identifierContext all_identifier() {
			return getRuleContext(All_identifierContext.class,0);
		}
		public Proto_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_proto_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterProto_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitProto_type(this);
		}
	}

	@RuleVersion(0)
	public final Proto_typeContext proto_type() throws RecognitionException {
		Proto_typeContext _localctx = new Proto_typeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_proto_type);
		try {
			setState(84);
			switch (_input.LA(1)) {
			case PROTOBUF_TYPE_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(82);
				match(PROTOBUF_TYPE_LITERAL);
				}
				break;
			case IDENTIFIER:
			case QUALIFIED_IDENTIFIER:
				enterOuterAlt(_localctx, 2);
				{
				setState(83);
				all_identifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProtoContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(ProtoParserParser.EOF, 0); }
		public List<? extends Package_defContext> package_def() {
			return getRuleContexts(Package_defContext.class);
		}
		public Package_defContext package_def(int i) {
			return getRuleContext(Package_defContext.class,i);
		}
		public List<? extends Import_defContext> import_def() {
			return getRuleContexts(Import_defContext.class);
		}
		public Import_defContext import_def(int i) {
			return getRuleContext(Import_defContext.class,i);
		}
		public List<? extends Syntax_defContext> syntax_def() {
			return getRuleContexts(Syntax_defContext.class);
		}
		public Syntax_defContext syntax_def(int i) {
			return getRuleContext(Syntax_defContext.class,i);
		}
		public List<? extends Option_line_defContext> option_line_def() {
			return getRuleContexts(Option_line_defContext.class);
		}
		public Option_line_defContext option_line_def(int i) {
			return getRuleContext(Option_line_defContext.class,i);
		}
		public List<? extends Enum_defContext> enum_def() {
			return getRuleContexts(Enum_defContext.class);
		}
		public Enum_defContext enum_def(int i) {
			return getRuleContext(Enum_defContext.class,i);
		}
		public List<? extends Ext_defContext> ext_def() {
			return getRuleContexts(Ext_defContext.class);
		}
		public Ext_defContext ext_def(int i) {
			return getRuleContext(Ext_defContext.class,i);
		}
		public List<? extends Message_defContext> message_def() {
			return getRuleContexts(Message_defContext.class);
		}
		public Message_defContext message_def(int i) {
			return getRuleContext(Message_defContext.class,i);
		}
		public List<? extends Service_defContext> service_def() {
			return getRuleContexts(Service_defContext.class);
		}
		public Service_defContext service_def(int i) {
			return getRuleContext(Service_defContext.class,i);
		}
		public ProtoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_proto; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterProto(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitProto(this);
		}
	}

	@RuleVersion(0)
	public final ProtoContext proto() throws RecognitionException {
		ProtoContext _localctx = new ProtoContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_proto);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PACKAGE_LITERAL) | (1L << IMPORT_LITERAL) | (1L << OPTION_LITERAL) | (1L << SYNTAX_LITERAL) | (1L << ENUM_LITERAL) | (1L << MESSAGE_LITERAL) | (1L << EXTEND_LITERAL) | (1L << SERVICE_LITERAL))) != 0)) {
				{
				setState(94);
				switch (_input.LA(1)) {
				case PACKAGE_LITERAL:
					{
					setState(86);
					package_def();
					}
					break;
				case IMPORT_LITERAL:
					{
					setState(87);
					import_def();
					}
					break;
				case SYNTAX_LITERAL:
					{
					setState(88);
					syntax_def();
					}
					break;
				case OPTION_LITERAL:
					{
					setState(89);
					option_line_def();
					}
					break;
				case ENUM_LITERAL:
					{
					setState(90);
					enum_def();
					}
					break;
				case EXTEND_LITERAL:
					{
					setState(91);
					ext_def();
					}
					break;
				case MESSAGE_LITERAL:
					{
					setState(92);
					message_def();
					}
					break;
				case SERVICE_LITERAL:
					{
					setState(93);
					service_def();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(98);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(99);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Package_defContext extends ParserRuleContext {
		public TerminalNode PACKAGE_LITERAL() { return getToken(ProtoParserParser.PACKAGE_LITERAL, 0); }
		public Package_nameContext package_name() {
			return getRuleContext(Package_nameContext.class,0);
		}
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public Package_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_package_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterPackage_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitPackage_def(this);
		}
	}

	@RuleVersion(0)
	public final Package_defContext package_def() throws RecognitionException {
		Package_defContext _localctx = new Package_defContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_package_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			match(PACKAGE_LITERAL);
			setState(102);
			package_name();
			setState(103);
			match(ITEM_TERMINATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Package_nameContext extends ParserRuleContext {
		public All_identifierContext all_identifier() {
			return getRuleContext(All_identifierContext.class,0);
		}
		public Package_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_package_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterPackage_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitPackage_name(this);
		}
	}

	@RuleVersion(0)
	public final Package_nameContext package_name() throws RecognitionException {
		Package_nameContext _localctx = new Package_nameContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_package_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(105);
			all_identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Import_defContext extends ParserRuleContext {
		public TerminalNode IMPORT_LITERAL() { return getToken(ProtoParserParser.IMPORT_LITERAL, 0); }
		public Import_file_nameContext import_file_name() {
			return getRuleContext(Import_file_nameContext.class,0);
		}
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public Import_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_import_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterImport_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitImport_def(this);
		}
	}

	@RuleVersion(0)
	public final Import_defContext import_def() throws RecognitionException {
		Import_defContext _localctx = new Import_defContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_import_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(IMPORT_LITERAL);
			setState(108);
			import_file_name();
			setState(109);
			match(ITEM_TERMINATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Import_file_nameContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(ProtoParserParser.STRING_LITERAL, 0); }
		public Import_file_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_import_file_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterImport_file_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitImport_file_name(this);
		}
	}

	@RuleVersion(0)
	public final Import_file_nameContext import_file_name() throws RecognitionException {
		Import_file_nameContext _localctx = new Import_file_nameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_import_file_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(STRING_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Syntax_defContext extends ParserRuleContext {
		public TerminalNode SYNTAX_LITERAL() { return getToken(ProtoParserParser.SYNTAX_LITERAL, 0); }
		public TerminalNode EQUALS() { return getToken(ProtoParserParser.EQUALS, 0); }
		public Syntax_nameContext syntax_name() {
			return getRuleContext(Syntax_nameContext.class,0);
		}
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public Syntax_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_syntax_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterSyntax_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitSyntax_def(this);
		}
	}

	@RuleVersion(0)
	public final Syntax_defContext syntax_def() throws RecognitionException {
		Syntax_defContext _localctx = new Syntax_defContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_syntax_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			match(SYNTAX_LITERAL);
			setState(114);
			match(EQUALS);
			setState(115);
			syntax_name();
			setState(116);
			match(ITEM_TERMINATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Syntax_nameContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(ProtoParserParser.STRING_LITERAL, 0); }
		public Syntax_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_syntax_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterSyntax_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitSyntax_name(this);
		}
	}

	@RuleVersion(0)
	public final Syntax_nameContext syntax_name() throws RecognitionException {
		Syntax_nameContext _localctx = new Syntax_nameContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_syntax_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			match(STRING_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Option_line_defContext extends ParserRuleContext {
		public TerminalNode OPTION_LITERAL() { return getToken(ProtoParserParser.OPTION_LITERAL, 0); }
		public Option_nameContext option_name() {
			return getRuleContext(Option_nameContext.class,0);
		}
		public TerminalNode EQUALS() { return getToken(ProtoParserParser.EQUALS, 0); }
		public Option_all_valueContext option_all_value() {
			return getRuleContext(Option_all_valueContext.class,0);
		}
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public Option_line_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option_line_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterOption_line_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitOption_line_def(this);
		}
	}

	@RuleVersion(0)
	public final Option_line_defContext option_line_def() throws RecognitionException {
		Option_line_defContext _localctx = new Option_line_defContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_option_line_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(120);
			match(OPTION_LITERAL);
			setState(121);
			option_name();
			setState(122);
			match(EQUALS);
			setState(123);
			option_all_value();
			setState(124);
			match(ITEM_TERMINATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Option_field_defContext extends ParserRuleContext {
		public TerminalNode BRACKET_OPEN() { return getToken(ProtoParserParser.BRACKET_OPEN, 0); }
		public List<? extends Option_field_itemContext> option_field_item() {
			return getRuleContexts(Option_field_itemContext.class);
		}
		public Option_field_itemContext option_field_item(int i) {
			return getRuleContext(Option_field_itemContext.class,i);
		}
		public TerminalNode BRACKET_CLOSE() { return getToken(ProtoParserParser.BRACKET_CLOSE, 0); }
		public List<? extends TerminalNode> COMMA() { return getTokens(ProtoParserParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(ProtoParserParser.COMMA, i);
		}
		public Option_field_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option_field_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterOption_field_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitOption_field_def(this);
		}
	}

	@RuleVersion(0)
	public final Option_field_defContext option_field_def() throws RecognitionException {
		Option_field_defContext _localctx = new Option_field_defContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_option_field_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126);
			match(BRACKET_OPEN);
			setState(127);
			option_field_item();
			setState(132);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(128);
				match(COMMA);
				setState(129);
				option_field_item();
				}
				}
				setState(134);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(135);
			match(BRACKET_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Option_field_itemContext extends ParserRuleContext {
		public Option_nameContext option_name() {
			return getRuleContext(Option_nameContext.class,0);
		}
		public TerminalNode EQUALS() { return getToken(ProtoParserParser.EQUALS, 0); }
		public Option_all_valueContext option_all_value() {
			return getRuleContext(Option_all_valueContext.class,0);
		}
		public Option_field_itemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option_field_item; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterOption_field_item(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitOption_field_item(this);
		}
	}

	@RuleVersion(0)
	public final Option_field_itemContext option_field_item() throws RecognitionException {
		Option_field_itemContext _localctx = new Option_field_itemContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_option_field_item);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			option_name();
			setState(138);
			match(EQUALS);
			setState(139);
			option_all_value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Option_all_valueContext extends ParserRuleContext {
		public All_valueContext all_value() {
			return getRuleContext(All_valueContext.class,0);
		}
		public Option_value_objectContext option_value_object() {
			return getRuleContext(Option_value_objectContext.class,0);
		}
		public Option_all_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option_all_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterOption_all_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitOption_all_value(this);
		}
	}

	@RuleVersion(0)
	public final Option_all_valueContext option_all_value() throws RecognitionException {
		Option_all_valueContext _localctx = new Option_all_valueContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_option_all_value);
		try {
			setState(143);
			switch (_input.LA(1)) {
			case INTEGER_LITERAL:
			case STRING_LITERAL:
			case BOOL_LITERAL:
			case FLOAT_LITERAL:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(141);
				all_value();
				}
				break;
			case BLOCK_OPEN:
				enterOuterAlt(_localctx, 2);
				{
				setState(142);
				option_value_object();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Option_value_objectContext extends ParserRuleContext {
		public TerminalNode BLOCK_OPEN() { return getToken(ProtoParserParser.BLOCK_OPEN, 0); }
		public TerminalNode BLOCK_CLOSE() { return getToken(ProtoParserParser.BLOCK_CLOSE, 0); }
		public List<? extends Option_value_itemContext> option_value_item() {
			return getRuleContexts(Option_value_itemContext.class);
		}
		public Option_value_itemContext option_value_item(int i) {
			return getRuleContext(Option_value_itemContext.class,i);
		}
		public Option_value_objectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option_value_object; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterOption_value_object(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitOption_value_object(this);
		}
	}

	@RuleVersion(0)
	public final Option_value_objectContext option_value_object() throws RecognitionException {
		Option_value_objectContext _localctx = new Option_value_objectContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_option_value_object);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			match(BLOCK_OPEN);
			setState(149);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IDENTIFIER) {
				{
				{
				setState(146);
				option_value_item();
				}
				}
				setState(151);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(152);
			match(BLOCK_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Option_value_itemContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public TerminalNode COLON() { return getToken(ProtoParserParser.COLON, 0); }
		public Option_all_valueContext option_all_value() {
			return getRuleContext(Option_all_valueContext.class,0);
		}
		public Option_value_itemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option_value_item; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterOption_value_item(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitOption_value_item(this);
		}
	}

	@RuleVersion(0)
	public final Option_value_itemContext option_value_item() throws RecognitionException {
		Option_value_itemContext _localctx = new Option_value_itemContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_option_value_item);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154);
			match(IDENTIFIER);
			setState(155);
			match(COLON);
			setState(156);
			option_all_value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Option_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public TerminalNode PAREN_OPEN() { return getToken(ProtoParserParser.PAREN_OPEN, 0); }
		public All_identifierContext all_identifier() {
			return getRuleContext(All_identifierContext.class,0);
		}
		public TerminalNode PAREN_CLOSE() { return getToken(ProtoParserParser.PAREN_CLOSE, 0); }
		public List<? extends TerminalNode> FIELD_IDENTIFIER() { return getTokens(ProtoParserParser.FIELD_IDENTIFIER); }
		public TerminalNode FIELD_IDENTIFIER(int i) {
			return getToken(ProtoParserParser.FIELD_IDENTIFIER, i);
		}
		public Option_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_option_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterOption_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitOption_name(this);
		}
	}

	@RuleVersion(0)
	public final Option_nameContext option_name() throws RecognitionException {
		Option_nameContext _localctx = new Option_nameContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_option_name);
		int _la;
		try {
			setState(168);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(158);
				match(IDENTIFIER);
				}
				break;
			case PAREN_OPEN:
				enterOuterAlt(_localctx, 2);
				{
				setState(159);
				match(PAREN_OPEN);
				setState(160);
				all_identifier();
				setState(161);
				match(PAREN_CLOSE);
				setState(165);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==FIELD_IDENTIFIER) {
					{
					{
					setState(162);
					match(FIELD_IDENTIFIER);
					}
					}
					setState(167);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enum_defContext extends ParserRuleContext {
		public TerminalNode ENUM_LITERAL() { return getToken(ProtoParserParser.ENUM_LITERAL, 0); }
		public Enum_nameContext enum_name() {
			return getRuleContext(Enum_nameContext.class,0);
		}
		public TerminalNode BLOCK_OPEN() { return getToken(ProtoParserParser.BLOCK_OPEN, 0); }
		public Enum_contentContext enum_content() {
			return getRuleContext(Enum_contentContext.class,0);
		}
		public TerminalNode BLOCK_CLOSE() { return getToken(ProtoParserParser.BLOCK_CLOSE, 0); }
		public Enum_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enum_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterEnum_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitEnum_def(this);
		}
	}

	@RuleVersion(0)
	public final Enum_defContext enum_def() throws RecognitionException {
		Enum_defContext _localctx = new Enum_defContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_enum_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			match(ENUM_LITERAL);
			setState(171);
			enum_name();
			setState(172);
			match(BLOCK_OPEN);
			setState(173);
			enum_content();
			setState(174);
			match(BLOCK_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enum_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public Enum_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enum_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterEnum_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitEnum_name(this);
		}
	}

	@RuleVersion(0)
	public final Enum_nameContext enum_name() throws RecognitionException {
		Enum_nameContext _localctx = new Enum_nameContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_enum_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(176);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enum_contentContext extends ParserRuleContext {
		public List<? extends Option_line_defContext> option_line_def() {
			return getRuleContexts(Option_line_defContext.class);
		}
		public Option_line_defContext option_line_def(int i) {
			return getRuleContext(Option_line_defContext.class,i);
		}
		public List<? extends Enum_item_defContext> enum_item_def() {
			return getRuleContexts(Enum_item_defContext.class);
		}
		public Enum_item_defContext enum_item_def(int i) {
			return getRuleContext(Enum_item_defContext.class,i);
		}
		public Enum_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enum_content; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterEnum_content(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitEnum_content(this);
		}
	}

	@RuleVersion(0)
	public final Enum_contentContext enum_content() throws RecognitionException {
		Enum_contentContext _localctx = new Enum_contentContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_enum_content);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(182);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OPTION_LITERAL || _la==IDENTIFIER) {
				{
				setState(180);
				switch (_input.LA(1)) {
				case OPTION_LITERAL:
					{
					setState(178);
					option_line_def();
					}
					break;
				case IDENTIFIER:
					{
					setState(179);
					enum_item_def();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(184);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enum_item_defContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public TerminalNode EQUALS() { return getToken(ProtoParserParser.EQUALS, 0); }
		public TerminalNode INTEGER_LITERAL() { return getToken(ProtoParserParser.INTEGER_LITERAL, 0); }
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public Option_field_defContext option_field_def() {
			return getRuleContext(Option_field_defContext.class,0);
		}
		public Enum_item_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enum_item_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterEnum_item_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitEnum_item_def(this);
		}
	}

	@RuleVersion(0)
	public final Enum_item_defContext enum_item_def() throws RecognitionException {
		Enum_item_defContext _localctx = new Enum_item_defContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_enum_item_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			match(IDENTIFIER);
			setState(186);
			match(EQUALS);
			setState(187);
			match(INTEGER_LITERAL);
			setState(189);
			_la = _input.LA(1);
			if (_la==BRACKET_OPEN) {
				{
				setState(188);
				option_field_def();
				}
			}

			setState(191);
			match(ITEM_TERMINATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Message_defContext extends ParserRuleContext {
		public TerminalNode MESSAGE_LITERAL() { return getToken(ProtoParserParser.MESSAGE_LITERAL, 0); }
		public Message_nameContext message_name() {
			return getRuleContext(Message_nameContext.class,0);
		}
		public TerminalNode BLOCK_OPEN() { return getToken(ProtoParserParser.BLOCK_OPEN, 0); }
		public TerminalNode BLOCK_CLOSE() { return getToken(ProtoParserParser.BLOCK_CLOSE, 0); }
		public Message_contentContext message_content() {
			return getRuleContext(Message_contentContext.class,0);
		}
		public Message_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_message_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterMessage_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitMessage_def(this);
		}
	}

	@RuleVersion(0)
	public final Message_defContext message_def() throws RecognitionException {
		Message_defContext _localctx = new Message_defContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_message_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193);
			match(MESSAGE_LITERAL);
			setState(194);
			message_name();
			setState(195);
			match(BLOCK_OPEN);
			setState(197);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPTION_LITERAL) | (1L << ENUM_LITERAL) | (1L << MESSAGE_LITERAL) | (1L << EXTENSIONS_DEF_LITERAL) | (1L << PROTOBUF_SCOPE_LITERAL))) != 0)) {
				{
				setState(196);
				message_content();
				}
			}

			setState(199);
			match(BLOCK_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Message_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public Message_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_message_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterMessage_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitMessage_name(this);
		}
	}

	@RuleVersion(0)
	public final Message_nameContext message_name() throws RecognitionException {
		Message_nameContext _localctx = new Message_nameContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_message_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(201);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Message_contentContext extends ParserRuleContext {
		public List<? extends Option_line_defContext> option_line_def() {
			return getRuleContexts(Option_line_defContext.class);
		}
		public Option_line_defContext option_line_def(int i) {
			return getRuleContext(Option_line_defContext.class,i);
		}
		public List<? extends Message_item_defContext> message_item_def() {
			return getRuleContexts(Message_item_defContext.class);
		}
		public Message_item_defContext message_item_def(int i) {
			return getRuleContext(Message_item_defContext.class,i);
		}
		public List<? extends Message_defContext> message_def() {
			return getRuleContexts(Message_defContext.class);
		}
		public Message_defContext message_def(int i) {
			return getRuleContext(Message_defContext.class,i);
		}
		public List<? extends Enum_defContext> enum_def() {
			return getRuleContexts(Enum_defContext.class);
		}
		public Enum_defContext enum_def(int i) {
			return getRuleContext(Enum_defContext.class,i);
		}
		public List<? extends Message_ext_defContext> message_ext_def() {
			return getRuleContexts(Message_ext_defContext.class);
		}
		public Message_ext_defContext message_ext_def(int i) {
			return getRuleContext(Message_ext_defContext.class,i);
		}
		public Message_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_message_content; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterMessage_content(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitMessage_content(this);
		}
	}

	@RuleVersion(0)
	public final Message_contentContext message_content() throws RecognitionException {
		Message_contentContext _localctx = new Message_contentContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_message_content);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(208); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(208);
				switch (_input.LA(1)) {
				case OPTION_LITERAL:
					{
					setState(203);
					option_line_def();
					}
					break;
				case PROTOBUF_SCOPE_LITERAL:
					{
					setState(204);
					message_item_def();
					}
					break;
				case MESSAGE_LITERAL:
					{
					setState(205);
					message_def();
					}
					break;
				case ENUM_LITERAL:
					{
					setState(206);
					enum_def();
					}
					break;
				case EXTENSIONS_DEF_LITERAL:
					{
					setState(207);
					message_ext_def();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(210); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPTION_LITERAL) | (1L << ENUM_LITERAL) | (1L << MESSAGE_LITERAL) | (1L << EXTENSIONS_DEF_LITERAL) | (1L << PROTOBUF_SCOPE_LITERAL))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Message_item_defContext extends ParserRuleContext {
		public TerminalNode PROTOBUF_SCOPE_LITERAL() { return getToken(ProtoParserParser.PROTOBUF_SCOPE_LITERAL, 0); }
		public Proto_typeContext proto_type() {
			return getRuleContext(Proto_typeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public TerminalNode EQUALS() { return getToken(ProtoParserParser.EQUALS, 0); }
		public TerminalNode INTEGER_LITERAL() { return getToken(ProtoParserParser.INTEGER_LITERAL, 0); }
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public Option_field_defContext option_field_def() {
			return getRuleContext(Option_field_defContext.class,0);
		}
		public Message_item_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_message_item_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterMessage_item_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitMessage_item_def(this);
		}
	}

	@RuleVersion(0)
	public final Message_item_defContext message_item_def() throws RecognitionException {
		Message_item_defContext _localctx = new Message_item_defContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_message_item_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(212);
			match(PROTOBUF_SCOPE_LITERAL);
			setState(213);
			proto_type();
			setState(214);
			match(IDENTIFIER);
			setState(215);
			match(EQUALS);
			setState(216);
			match(INTEGER_LITERAL);
			setState(218);
			_la = _input.LA(1);
			if (_la==BRACKET_OPEN) {
				{
				setState(217);
				option_field_def();
				}
			}

			setState(220);
			match(ITEM_TERMINATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Message_ext_defContext extends ParserRuleContext {
		public Token v;
		public TerminalNode EXTENSIONS_DEF_LITERAL() { return getToken(ProtoParserParser.EXTENSIONS_DEF_LITERAL, 0); }
		public List<? extends TerminalNode> INTEGER_LITERAL() { return getTokens(ProtoParserParser.INTEGER_LITERAL); }
		public TerminalNode INTEGER_LITERAL(int i) {
			return getToken(ProtoParserParser.INTEGER_LITERAL, i);
		}
		public TerminalNode EXTENSIONS_TO_LITERAL() { return getToken(ProtoParserParser.EXTENSIONS_TO_LITERAL, 0); }
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public TerminalNode EXTENSIONS_MAX_LITERAL() { return getToken(ProtoParserParser.EXTENSIONS_MAX_LITERAL, 0); }
		public Message_ext_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_message_ext_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterMessage_ext_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitMessage_ext_def(this);
		}
	}

	@RuleVersion(0)
	public final Message_ext_defContext message_ext_def() throws RecognitionException {
		Message_ext_defContext _localctx = new Message_ext_defContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_message_ext_def);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			match(EXTENSIONS_DEF_LITERAL);
			setState(223);
			match(INTEGER_LITERAL);
			setState(224);
			match(EXTENSIONS_TO_LITERAL);
			setState(227);
			switch (_input.LA(1)) {
			case INTEGER_LITERAL:
				{
				setState(225);
				_localctx.v = match(INTEGER_LITERAL);
				}
				break;
			case EXTENSIONS_MAX_LITERAL:
				{
				setState(226);
				_localctx.v = match(EXTENSIONS_MAX_LITERAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(229);
			match(ITEM_TERMINATOR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ext_defContext extends ParserRuleContext {
		public TerminalNode EXTEND_LITERAL() { return getToken(ProtoParserParser.EXTEND_LITERAL, 0); }
		public Ext_nameContext ext_name() {
			return getRuleContext(Ext_nameContext.class,0);
		}
		public TerminalNode BLOCK_OPEN() { return getToken(ProtoParserParser.BLOCK_OPEN, 0); }
		public TerminalNode BLOCK_CLOSE() { return getToken(ProtoParserParser.BLOCK_CLOSE, 0); }
		public Ext_contentContext ext_content() {
			return getRuleContext(Ext_contentContext.class,0);
		}
		public Ext_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ext_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterExt_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitExt_def(this);
		}
	}

	@RuleVersion(0)
	public final Ext_defContext ext_def() throws RecognitionException {
		Ext_defContext _localctx = new Ext_defContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_ext_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(231);
			match(EXTEND_LITERAL);
			setState(232);
			ext_name();
			setState(233);
			match(BLOCK_OPEN);
			setState(235);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPTION_LITERAL) | (1L << ENUM_LITERAL) | (1L << MESSAGE_LITERAL) | (1L << PROTOBUF_SCOPE_LITERAL))) != 0)) {
				{
				setState(234);
				ext_content();
				}
			}

			setState(237);
			match(BLOCK_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ext_nameContext extends ParserRuleContext {
		public All_identifierContext all_identifier() {
			return getRuleContext(All_identifierContext.class,0);
		}
		public Ext_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ext_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterExt_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitExt_name(this);
		}
	}

	@RuleVersion(0)
	public final Ext_nameContext ext_name() throws RecognitionException {
		Ext_nameContext _localctx = new Ext_nameContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_ext_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			all_identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ext_contentContext extends ParserRuleContext {
		public List<? extends Option_line_defContext> option_line_def() {
			return getRuleContexts(Option_line_defContext.class);
		}
		public Option_line_defContext option_line_def(int i) {
			return getRuleContext(Option_line_defContext.class,i);
		}
		public List<? extends Message_item_defContext> message_item_def() {
			return getRuleContexts(Message_item_defContext.class);
		}
		public Message_item_defContext message_item_def(int i) {
			return getRuleContext(Message_item_defContext.class,i);
		}
		public List<? extends Message_defContext> message_def() {
			return getRuleContexts(Message_defContext.class);
		}
		public Message_defContext message_def(int i) {
			return getRuleContext(Message_defContext.class,i);
		}
		public List<? extends Enum_defContext> enum_def() {
			return getRuleContexts(Enum_defContext.class);
		}
		public Enum_defContext enum_def(int i) {
			return getRuleContext(Enum_defContext.class,i);
		}
		public Ext_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ext_content; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterExt_content(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitExt_content(this);
		}
	}

	@RuleVersion(0)
	public final Ext_contentContext ext_content() throws RecognitionException {
		Ext_contentContext _localctx = new Ext_contentContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_ext_content);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(245); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(245);
				switch (_input.LA(1)) {
				case OPTION_LITERAL:
					{
					setState(241);
					option_line_def();
					}
					break;
				case PROTOBUF_SCOPE_LITERAL:
					{
					setState(242);
					message_item_def();
					}
					break;
				case MESSAGE_LITERAL:
					{
					setState(243);
					message_def();
					}
					break;
				case ENUM_LITERAL:
					{
					setState(244);
					enum_def();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(247); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OPTION_LITERAL) | (1L << ENUM_LITERAL) | (1L << MESSAGE_LITERAL) | (1L << PROTOBUF_SCOPE_LITERAL))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Service_defContext extends ParserRuleContext {
		public TerminalNode SERVICE_LITERAL() { return getToken(ProtoParserParser.SERVICE_LITERAL, 0); }
		public Service_nameContext service_name() {
			return getRuleContext(Service_nameContext.class,0);
		}
		public TerminalNode BLOCK_OPEN() { return getToken(ProtoParserParser.BLOCK_OPEN, 0); }
		public TerminalNode BLOCK_CLOSE() { return getToken(ProtoParserParser.BLOCK_CLOSE, 0); }
		public Service_contentContext service_content() {
			return getRuleContext(Service_contentContext.class,0);
		}
		public Service_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_service_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterService_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitService_def(this);
		}
	}

	@RuleVersion(0)
	public final Service_defContext service_def() throws RecognitionException {
		Service_defContext _localctx = new Service_defContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_service_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(249);
			match(SERVICE_LITERAL);
			setState(250);
			service_name();
			setState(251);
			match(BLOCK_OPEN);
			setState(253);
			_la = _input.LA(1);
			if (_la==OPTION_LITERAL || _la==RPC_LITERAL) {
				{
				setState(252);
				service_content();
				}
			}

			setState(255);
			match(BLOCK_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Service_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public Service_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_service_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterService_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitService_name(this);
		}
	}

	@RuleVersion(0)
	public final Service_nameContext service_name() throws RecognitionException {
		Service_nameContext _localctx = new Service_nameContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_service_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(257);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Service_contentContext extends ParserRuleContext {
		public List<? extends Option_line_defContext> option_line_def() {
			return getRuleContexts(Option_line_defContext.class);
		}
		public Option_line_defContext option_line_def(int i) {
			return getRuleContext(Option_line_defContext.class,i);
		}
		public List<? extends Rpc_defContext> rpc_def() {
			return getRuleContexts(Rpc_defContext.class);
		}
		public Rpc_defContext rpc_def(int i) {
			return getRuleContext(Rpc_defContext.class,i);
		}
		public Service_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_service_content; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterService_content(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitService_content(this);
		}
	}

	@RuleVersion(0)
	public final Service_contentContext service_content() throws RecognitionException {
		Service_contentContext _localctx = new Service_contentContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_service_content);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(261);
				switch (_input.LA(1)) {
				case OPTION_LITERAL:
					{
					setState(259);
					option_line_def();
					}
					break;
				case RPC_LITERAL:
					{
					setState(260);
					rpc_def();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(263); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==OPTION_LITERAL || _la==RPC_LITERAL );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rpc_defContext extends ParserRuleContext {
		public TerminalNode RPC_LITERAL() { return getToken(ProtoParserParser.RPC_LITERAL, 0); }
		public Rpc_nameContext rpc_name() {
			return getRuleContext(Rpc_nameContext.class,0);
		}
		public List<? extends TerminalNode> PAREN_OPEN() { return getTokens(ProtoParserParser.PAREN_OPEN); }
		public TerminalNode PAREN_OPEN(int i) {
			return getToken(ProtoParserParser.PAREN_OPEN, i);
		}
		public Req_nameContext req_name() {
			return getRuleContext(Req_nameContext.class,0);
		}
		public List<? extends TerminalNode> PAREN_CLOSE() { return getTokens(ProtoParserParser.PAREN_CLOSE); }
		public TerminalNode PAREN_CLOSE(int i) {
			return getToken(ProtoParserParser.PAREN_CLOSE, i);
		}
		public TerminalNode RETURNS_LITERAL() { return getToken(ProtoParserParser.RETURNS_LITERAL, 0); }
		public Resp_nameContext resp_name() {
			return getRuleContext(Resp_nameContext.class,0);
		}
		public TerminalNode BLOCK_OPEN() { return getToken(ProtoParserParser.BLOCK_OPEN, 0); }
		public TerminalNode BLOCK_CLOSE() { return getToken(ProtoParserParser.BLOCK_CLOSE, 0); }
		public TerminalNode ITEM_TERMINATOR() { return getToken(ProtoParserParser.ITEM_TERMINATOR, 0); }
		public List<? extends Option_line_defContext> option_line_def() {
			return getRuleContexts(Option_line_defContext.class);
		}
		public Option_line_defContext option_line_def(int i) {
			return getRuleContext(Option_line_defContext.class,i);
		}
		public Rpc_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rpc_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterRpc_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitRpc_def(this);
		}
	}

	@RuleVersion(0)
	public final Rpc_defContext rpc_def() throws RecognitionException {
		Rpc_defContext _localctx = new Rpc_defContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_rpc_def);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(265);
			match(RPC_LITERAL);
			setState(266);
			rpc_name();
			setState(267);
			match(PAREN_OPEN);
			setState(268);
			req_name();
			setState(269);
			match(PAREN_CLOSE);
			setState(270);
			match(RETURNS_LITERAL);
			setState(271);
			match(PAREN_OPEN);
			setState(272);
			resp_name();
			setState(273);
			match(PAREN_CLOSE);
			setState(286);
			switch (_input.LA(1)) {
			case BLOCK_OPEN:
				{
				setState(274);
				match(BLOCK_OPEN);
				setState(278);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==OPTION_LITERAL) {
					{
					{
					setState(275);
					option_line_def();
					}
					}
					setState(280);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(281);
				match(BLOCK_CLOSE);
				setState(283);
				_la = _input.LA(1);
				if (_la==ITEM_TERMINATOR) {
					{
					setState(282);
					match(ITEM_TERMINATOR);
					}
				}

				}
				break;
			case ITEM_TERMINATOR:
				{
				setState(285);
				match(ITEM_TERMINATOR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rpc_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ProtoParserParser.IDENTIFIER, 0); }
		public Rpc_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rpc_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterRpc_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitRpc_name(this);
		}
	}

	@RuleVersion(0)
	public final Rpc_nameContext rpc_name() throws RecognitionException {
		Rpc_nameContext _localctx = new Rpc_nameContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_rpc_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(288);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Req_nameContext extends ParserRuleContext {
		public All_identifierContext all_identifier() {
			return getRuleContext(All_identifierContext.class,0);
		}
		public Req_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_req_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterReq_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitReq_name(this);
		}
	}

	@RuleVersion(0)
	public final Req_nameContext req_name() throws RecognitionException {
		Req_nameContext _localctx = new Req_nameContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_req_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(290);
			all_identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Resp_nameContext extends ParserRuleContext {
		public All_identifierContext all_identifier() {
			return getRuleContext(All_identifierContext.class,0);
		}
		public Resp_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resp_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).enterResp_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ProtoParserListener ) ((ProtoParserListener)listener).exitResp_name(this);
		}
	}

	@RuleVersion(0)
	public final Resp_nameContext resp_name() throws RecognitionException {
		Resp_nameContext _localctx = new Resp_nameContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_resp_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(292);
			all_identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uaf6f\u8320\u479d\ub75c\u4880\u1605\u191c\uab37\3%\u0129\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\3\2\3\2\3\3\3\3\5\3Q\n\3\3\4\3\4\3"+
		"\5\3\5\5\5W\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6a\n\6\f\6\16\6d\13"+
		"\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\13\3\13\3"+
		"\13\3\13\3\13\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\7\16"+
		"\u0085\n\16\f\16\16\16\u0088\13\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20"+
		"\3\20\5\20\u0092\n\20\3\21\3\21\7\21\u0096\n\21\f\21\16\21\u0099\13\21"+
		"\3\21\3\21\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\7\23\u00a6\n\23"+
		"\f\23\16\23\u00a9\13\23\5\23\u00ab\n\23\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\25\3\25\3\26\3\26\7\26\u00b7\n\26\f\26\16\26\u00ba\13\26\3\27\3\27"+
		"\3\27\3\27\5\27\u00c0\n\27\3\27\3\27\3\30\3\30\3\30\3\30\5\30\u00c8\n"+
		"\30\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\32\3\32\6\32\u00d3\n\32\r\32"+
		"\16\32\u00d4\3\33\3\33\3\33\3\33\3\33\3\33\5\33\u00dd\n\33\3\33\3\33\3"+
		"\34\3\34\3\34\3\34\3\34\5\34\u00e6\n\34\3\34\3\34\3\35\3\35\3\35\3\35"+
		"\5\35\u00ee\n\35\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3\37\6\37\u00f8\n"+
		"\37\r\37\16\37\u00f9\3 \3 \3 \3 \5 \u0100\n \3 \3 \3!\3!\3\"\3\"\6\"\u0108"+
		"\n\"\r\"\16\"\u0109\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\7#\u0117\n#\f#\16"+
		"#\u011a\13#\3#\3#\5#\u011e\n#\3#\5#\u0121\n#\3$\3$\3%\3%\3&\3&\3&\2\2"+
		"\2\'\2\2\4\2\6\2\b\2\n\2\f\2\16\2\20\2\22\2\24\2\26\2\30\2\32\2\34\2\36"+
		"\2 \2\"\2$\2&\2(\2*\2,\2.\2\60\2\62\2\64\2\66\28\2:\2<\2>\2@\2B\2D\2F"+
		"\2H\2J\2\2\4\3\2 !\3\2\34\37\u0128\2L\3\2\2\2\4P\3\2\2\2\6R\3\2\2\2\b"+
		"V\3\2\2\2\nb\3\2\2\2\fg\3\2\2\2\16k\3\2\2\2\20m\3\2\2\2\22q\3\2\2\2\24"+
		"s\3\2\2\2\26x\3\2\2\2\30z\3\2\2\2\32\u0080\3\2\2\2\34\u008b\3\2\2\2\36"+
		"\u0091\3\2\2\2 \u0093\3\2\2\2\"\u009c\3\2\2\2$\u00aa\3\2\2\2&\u00ac\3"+
		"\2\2\2(\u00b2\3\2\2\2*\u00b8\3\2\2\2,\u00bb\3\2\2\2.\u00c3\3\2\2\2\60"+
		"\u00cb\3\2\2\2\62\u00d2\3\2\2\2\64\u00d6\3\2\2\2\66\u00e0\3\2\2\28\u00e9"+
		"\3\2\2\2:\u00f1\3\2\2\2<\u00f7\3\2\2\2>\u00fb\3\2\2\2@\u0103\3\2\2\2B"+
		"\u0107\3\2\2\2D\u010b\3\2\2\2F\u0122\3\2\2\2H\u0124\3\2\2\2J\u0126\3\2"+
		"\2\2LM\t\2\2\2M\3\3\2\2\2NQ\7 \2\2OQ\5\6\4\2PN\3\2\2\2PO\3\2\2\2Q\5\3"+
		"\2\2\2RS\t\3\2\2S\7\3\2\2\2TW\7\33\2\2UW\5\2\2\2VT\3\2\2\2VU\3\2\2\2W"+
		"\t\3\2\2\2Xa\5\f\7\2Ya\5\20\t\2Za\5\24\13\2[a\5\30\r\2\\a\5&\24\2]a\5"+
		"8\35\2^a\5.\30\2_a\5> \2`X\3\2\2\2`Y\3\2\2\2`Z\3\2\2\2`[\3\2\2\2`\\\3"+
		"\2\2\2`]\3\2\2\2`^\3\2\2\2`_\3\2\2\2ad\3\2\2\2b`\3\2\2\2bc\3\2\2\2ce\3"+
		"\2\2\2db\3\2\2\2ef\7\2\2\3f\13\3\2\2\2gh\7\3\2\2hi\5\16\b\2ij\7\31\2\2"+
		"j\r\3\2\2\2kl\5\2\2\2l\17\3\2\2\2mn\7\4\2\2no\5\22\n\2op\7\31\2\2p\21"+
		"\3\2\2\2qr\7\35\2\2r\23\3\2\2\2st\7\6\2\2tu\7\26\2\2uv\5\26\f\2vw\7\31"+
		"\2\2w\25\3\2\2\2xy\7\35\2\2y\27\3\2\2\2z{\7\5\2\2{|\5$\23\2|}\7\26\2\2"+
		"}~\5\36\20\2~\177\7\31\2\2\177\31\3\2\2\2\u0080\u0081\7\24\2\2\u0081\u0086"+
		"\5\34\17\2\u0082\u0083\7\30\2\2\u0083\u0085\5\34\17\2\u0084\u0082\3\2"+
		"\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2\2\2\u0086\u0087\3\2\2\2\u0087"+
		"\u0089\3\2\2\2\u0088\u0086\3\2\2\2\u0089\u008a\7\25\2\2\u008a\33\3\2\2"+
		"\2\u008b\u008c\5$\23\2\u008c\u008d\7\26\2\2\u008d\u008e\5\36\20\2\u008e"+
		"\35\3\2\2\2\u008f\u0092\5\4\3\2\u0090\u0092\5 \21\2\u0091\u008f\3\2\2"+
		"\2\u0091\u0090\3\2\2\2\u0092\37\3\2\2\2\u0093\u0097\7\20\2\2\u0094\u0096"+
		"\5\"\22\2\u0095\u0094\3\2\2\2\u0096\u0099\3\2\2\2\u0097\u0095\3\2\2\2"+
		"\u0097\u0098\3\2\2\2\u0098\u009a\3\2\2\2\u0099\u0097\3\2\2\2\u009a\u009b"+
		"\7\21\2\2\u009b!\3\2\2\2\u009c\u009d\7 \2\2\u009d\u009e\7\27\2\2\u009e"+
		"\u009f\5\36\20\2\u009f#\3\2\2\2\u00a0\u00ab\7 \2\2\u00a1\u00a2\7\22\2"+
		"\2\u00a2\u00a3\5\2\2\2\u00a3\u00a7\7\23\2\2\u00a4\u00a6\7\"\2\2\u00a5"+
		"\u00a4\3\2\2\2\u00a6\u00a9\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2"+
		"\2\2\u00a8\u00ab\3\2\2\2\u00a9\u00a7\3\2\2\2\u00aa\u00a0\3\2\2\2\u00aa"+
		"\u00a1\3\2\2\2\u00ab%\3\2\2\2\u00ac\u00ad\7\7\2\2\u00ad\u00ae\5(\25\2"+
		"\u00ae\u00af\7\20\2\2\u00af\u00b0\5*\26\2\u00b0\u00b1\7\21\2\2\u00b1\'"+
		"\3\2\2\2\u00b2\u00b3\7 \2\2\u00b3)\3\2\2\2\u00b4\u00b7\5\30\r\2\u00b5"+
		"\u00b7\5,\27\2\u00b6\u00b4\3\2\2\2\u00b6\u00b5\3\2\2\2\u00b7\u00ba\3\2"+
		"\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9+\3\2\2\2\u00ba\u00b8"+
		"\3\2\2\2\u00bb\u00bc\7 \2\2\u00bc\u00bd\7\26\2\2\u00bd\u00bf\7\34\2\2"+
		"\u00be\u00c0\5\32\16\2\u00bf\u00be\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c1"+
		"\3\2\2\2\u00c1\u00c2\7\31\2\2\u00c2-\3\2\2\2\u00c3\u00c4\7\b\2\2\u00c4"+
		"\u00c5\5\60\31\2\u00c5\u00c7\7\20\2\2\u00c6\u00c8\5\62\32\2\u00c7\u00c6"+
		"\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ca\7\21\2\2"+
		"\u00ca/\3\2\2\2\u00cb\u00cc\7 \2\2\u00cc\61\3\2\2\2\u00cd\u00d3\5\30\r"+
		"\2\u00ce\u00d3\5\64\33\2\u00cf\u00d3\5.\30\2\u00d0\u00d3\5&\24\2\u00d1"+
		"\u00d3\5\66\34\2\u00d2\u00cd\3\2\2\2\u00d2\u00ce\3\2\2\2\u00d2\u00cf\3"+
		"\2\2\2\u00d2\u00d0\3\2\2\2\u00d2\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4"+
		"\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\63\3\2\2\2\u00d6\u00d7\7\32\2"+
		"\2\u00d7\u00d8\5\b\5\2\u00d8\u00d9\7 \2\2\u00d9\u00da\7\26\2\2\u00da\u00dc"+
		"\7\34\2\2\u00db\u00dd\5\32\16\2\u00dc\u00db\3\2\2\2\u00dc\u00dd\3\2\2"+
		"\2\u00dd\u00de\3\2\2\2\u00de\u00df\7\31\2\2\u00df\65\3\2\2\2\u00e0\u00e1"+
		"\7\n\2\2\u00e1\u00e2\7\34\2\2\u00e2\u00e5\7\13\2\2\u00e3\u00e6\7\34\2"+
		"\2\u00e4\u00e6\7\f\2\2\u00e5\u00e3\3\2\2\2\u00e5\u00e4\3\2\2\2\u00e6\u00e7"+
		"\3\2\2\2\u00e7\u00e8\7\31\2\2\u00e8\67\3\2\2\2\u00e9\u00ea\7\t\2\2\u00ea"+
		"\u00eb\5:\36\2\u00eb\u00ed\7\20\2\2\u00ec\u00ee\5<\37\2\u00ed\u00ec\3"+
		"\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f0\7\21\2\2\u00f0"+
		"9\3\2\2\2\u00f1\u00f2\5\2\2\2\u00f2;\3\2\2\2\u00f3\u00f8\5\30\r\2\u00f4"+
		"\u00f8\5\64\33\2\u00f5\u00f8\5.\30\2\u00f6\u00f8\5&\24\2\u00f7\u00f3\3"+
		"\2\2\2\u00f7\u00f4\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f7\u00f6\3\2\2\2\u00f8"+
		"\u00f9\3\2\2\2\u00f9\u00f7\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa=\3\2\2\2"+
		"\u00fb\u00fc\7\r\2\2\u00fc\u00fd\5@!\2\u00fd\u00ff\7\20\2\2\u00fe\u0100"+
		"\5B\"\2\u00ff\u00fe\3\2\2\2\u00ff\u0100\3\2\2\2\u0100\u0101\3\2\2\2\u0101"+
		"\u0102\7\21\2\2\u0102?\3\2\2\2\u0103\u0104\7 \2\2\u0104A\3\2\2\2\u0105"+
		"\u0108\5\30\r\2\u0106\u0108\5D#\2\u0107\u0105\3\2\2\2\u0107\u0106\3\2"+
		"\2\2\u0108\u0109\3\2\2\2\u0109\u0107\3\2\2\2\u0109\u010a\3\2\2\2\u010a"+
		"C\3\2\2\2\u010b\u010c\7\17\2\2\u010c\u010d\5F$\2\u010d\u010e\7\22\2\2"+
		"\u010e\u010f\5H%\2\u010f\u0110\7\23\2\2\u0110\u0111\7\16\2\2\u0111\u0112"+
		"\7\22\2\2\u0112\u0113\5J&\2\u0113\u0120\7\23\2\2\u0114\u0118\7\20\2\2"+
		"\u0115\u0117\5\30\r\2\u0116\u0115\3\2\2\2\u0117\u011a\3\2\2\2\u0118\u0116"+
		"\3\2\2\2\u0118\u0119\3\2\2\2\u0119\u011b\3\2\2\2\u011a\u0118\3\2\2\2\u011b"+
		"\u011d\7\21\2\2\u011c\u011e\7\31\2\2\u011d\u011c\3\2\2\2\u011d\u011e\3"+
		"\2\2\2\u011e\u0121\3\2\2\2\u011f\u0121\7\31\2\2\u0120\u0114\3\2\2\2\u0120"+
		"\u011f\3\2\2\2\u0121E\3\2\2\2\u0122\u0123\7 \2\2\u0123G\3\2\2\2\u0124"+
		"\u0125\5\2\2\2\u0125I\3\2\2\2\u0126\u0127\5\2\2\2\u0127K\3\2\2\2\34PV"+
		"`b\u0086\u0091\u0097\u00a7\u00aa\u00b6\u00b8\u00bf\u00c7\u00d2\u00d4\u00dc"+
		"\u00e5\u00ed\u00f7\u00f9\u00ff\u0107\u0109\u0118\u011d\u0120";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
	}
}